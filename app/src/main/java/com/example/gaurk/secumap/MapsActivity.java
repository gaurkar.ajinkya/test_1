package com.example.gaurk.secumap;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private View circle_view;
    private ImageView ctr_pin;
    private FloatingActionButton area_fab;
    private FloatingActionButton search_fab;
    private FloatingActionButton pin_fab;
    private FloatingActionsMenu menuMultipleActions;
    private DiscreteSeekBar yearSeekBar;
    private RelativeLayout heat_map_layout;
    private Button select_cood_btn;
    private Button cancel_cood_btn;
    private RelativeLayout selection_confirm_layout;
    private LatLng center_latlong;
    private TextView selected_latlong_text_view;
    private FloatingActionButton filterFab;
    private FloatingActionButton add_item_Fab;
    private ImageView options_image_view;
    private HeatmapTileProvider mProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        circle_view = (View) findViewById(R.id.circle_view);
        ctr_pin = (ImageView) findViewById(R.id.center_pin);
        options_image_view = (ImageView) findViewById(R.id.options_image_view);
//        yearSeekBar = (DiscreteSeekBar) findViewById(R.id.discreteSeekBar);
        heat_map_layout = (RelativeLayout) findViewById(R.id.heat_map_layout);
        selection_confirm_layout = (RelativeLayout) findViewById(R.id.selection_confirm_layout);
        select_cood_btn = (Button) findViewById(R.id.select_cood_btn);
        cancel_cood_btn = (Button) findViewById(R.id.cancel_btn);
        selected_latlong_text_view = (TextView) findViewById(R.id.selected_latlong);

//        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.add_btn_list);
        filterFab = (FloatingActionButton) findViewById(R.id.filter_fab);
        add_item_Fab = (FloatingActionButton) findViewById(R.id.add_item_btn);

//        search_fab = (FloatingActionButton) findViewById(R.id.search_fab);
//        pin_fab = (FloatingActionButton) findViewById(R.id.pin_fab);
//        area_fab = (FloatingActionButton) findViewById(R.id.area_fab);

        add_item_Fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctr_pin.setVisibility(View.VISIBLE);
                circle_view.setVisibility(View.INVISIBLE);
                heat_map_layout.setVisibility(View.INVISIBLE);
                selection_confirm_layout.setVisibility(View.VISIBLE);
            }
        });
        filterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater flater = LayoutInflater.from(MapsActivity.this);

                View addItemDialogView = flater.inflate(R.layout.filter_layout, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                builder.setView(addItemDialogView);
                builder.setCancelable(false);
                EditText item_name_edit_text = (EditText) addItemDialogView.findViewById(R.id.incident_header_text_view);
                EditText item_detail_edit_text = (EditText) addItemDialogView.findViewById(R.id.new_item_detail_EditText);
                RadioGroup qualification_radio_group = (RadioGroup) addItemDialogView.findViewById(R.id.qualification_radio_group);

                builder.setTitle("Add filter");
                builder.setPositiveButton("Filter", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("XXXXXX ");
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("YYYYYY : ");

                    }
                });
                builder.show();
            }
        });
        options_image_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("growthMenuImageView  growthMenuImageView  growthMenuImageView  growthMenuImageView");
                PopupMenu popup = new PopupMenu(MapsActivity.this, options_image_view);
                popup.getMenuInflater().inflate(R.menu.main_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("Statistics")) {
                            Intent sharewIntent = new Intent(MapsActivity.this, Statistics_main.class);
                            startActivity(sharewIntent);
                        }
//                         else if (item.getTitle().equals("Screen Shot")) {
//                            Intent sharewIntent = new Intent(Growth_main.this, Growth_overview_data.class);
//                            startActivity(sharewIntent);
//                        }

                        return false;
                    }
                });
                popup.show();
            }
        });
//        search_fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ctr_pin.setVisibility(View.INVISIBLE);
//                circle_view.setVisibility(View.INVISIBLE);
//                heat_map_layout.setVisibility(View.INVISIBLE);
//                menuMultipleActions.collapse();
//                selection_confirm_layout.setVisibility(View.VISIBLE);
//            }
//        });
//        area_fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ctr_pin.setVisibility(View.INVISIBLE);
//                heat_map_layout.setVisibility(View.INVISIBLE);
//                circle_view.setVisibility(View.VISIBLE);
//                menuMultipleActions.collapse();
//                selection_confirm_layout.setVisibility(View.VISIBLE);
//            }
//        });

        cancel_cood_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                heat_map_layout.setVisibility(View.VISIBLE);
                selection_confirm_layout.setVisibility(View.INVISIBLE);
            }
        });
        select_cood_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LayoutInflater flater = LayoutInflater.from(MapsActivity.this);

                View addItemDialogView = flater.inflate(R.layout.item_add_layout, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                builder.setView(addItemDialogView);
                builder.setCancelable(false);
                EditText item_name_edit_text = (EditText) addItemDialogView.findViewById(R.id.incident_header_text_view);
                EditText item_detail_edit_text = (EditText) addItemDialogView.findViewById(R.id.new_item_detail_EditText);
                RadioGroup qualification_radio_group = (RadioGroup) addItemDialogView.findViewById(R.id.qualification_radio_group);

                builder.setTitle("New Input");
                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("XXXXXX ");
                    }
                });
                builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("YYYYYY : ");

                    }
                });
                builder.show();

            }
        });

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng centennial_High_School = new LatLng(39.294541, -76.613114);
        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.test_bmp);
//        mMap.addMarker(new MarkerOptions().position(centennial_High_School).title("centennial High School"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(centennial_High_School, 14.0f));
//        map.moveCamera( CameraUpdateFactory.newLatLngZoom(UMBC , 14.0f) );

        center_latlong = mMap.getCameraPosition().target;
        System.out.println("center_latlong : " + center_latlong);

        float zoom = mMap.getCameraPosition().zoom;
        System.out.println("zoom : " + zoom);


        LatLng police_stn_1 = new LatLng(39.322534, -76.602527);
        LatLng police_stn_2 = new LatLng(39.303205, -76.666369);
        LatLng police_stn_3 = new LatLng(39.278496, -76.625908);
        LatLng police_stn_4 = new LatLng(39.343984, -76.640925);
        LatLng hosp_1 = new LatLng(39.317798, -76.614788);
        LatLng hosp_2 = new LatLng(39.303466, -76.631717);
        LatLng hosp_3 = new LatLng(39.288018, -76.624812);
        LatLng hosp_4 = new LatLng(39.292341, -76.596476);
        LatLng hosp_5 = new LatLng(39.288679, -76.649355);
        LatLng hosp_6 = new LatLng(39.303466, -76.631717);
        LatLng hosp_7 = new LatLng(39.345209, -76.622208);
        LatLng hosp_8 = new LatLng(39.307385, -76.680034);
        LatLng camp_1 = new LatLng(39.346048, -76.609995);
        LatLng camp_2 = new LatLng(39.280838, -76.671105);
        LatLng camp_3 = new LatLng(39.290820, -76.576817);
        mMap.addMarker(new MarkerOptions().position(police_stn_1).title("Police station, 21334").icon(BitmapDescriptorFactory.fromResource(R.drawable.police_station_32)));
        mMap.addMarker(new MarkerOptions().position(police_stn_2).title("Police station, 21774").icon(BitmapDescriptorFactory.fromResource(R.drawable.police_station_32)));
        mMap.addMarker(new MarkerOptions().position(police_stn_3).title("Police station, 21227").icon(BitmapDescriptorFactory.fromResource(R.drawable.police_station_32)));
        mMap.addMarker(new MarkerOptions().position(police_stn_4).title("Police station, 21212").icon(BitmapDescriptorFactory.fromResource(R.drawable.police_station_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_1).title("Care center, 21526").snippet("Open today, 10am - 5pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_2).title("Care center, 21142").snippet("Open today, 08am - 9pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_3).title("Care center, 21312").snippet("Open today, 10am - 3pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_4).title("Care center, 21251").snippet("Open today, 08am - 5pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_5).title("Care center, 21116").snippet("Open today, 10am - 6pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_6).title("Care center, 21172").snippet("Open today, 09am - 5pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_7).title("Care center, 21635").snippet("Open today, 10am - 5pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(hosp_8).title("Care center, 21113").snippet("Open today, 10am - 5pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_green_32)));
        mMap.addMarker(new MarkerOptions().position(camp_3).title("Drug rehab event, 21227").snippet("Oct 27, 11am - 5pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_32_trai)));
        mMap.addMarker(new MarkerOptions().position(camp_2).title("Public Naloxone Training, 21213").snippet("Oct 27, 9:30am - 11:30pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_32_trai))).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(camp_1).title("Drug rehab Event, 21223").snippet("Oct 22, 1pm - 3pm").icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_32_trai)));
//
//        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            @Override
//            public View getInfoContents(Marker marker) {
//
//                View v = getLayoutInflater().inflate(R.layout.marker, null);
//
//                TextView info = (TextView) v.findViewById(R.id.info);
//
//                info.setText(marker.getPosition().toString());
//
//                return v;
//            }
//        });

        mMap.addCircle(new CircleOptions().center(new LatLng(39.328345, -76.621778)).radius(1000).strokeColor(Color.parseColor("#00ff5b60")).fillColor(Color.parseColor("#90ff5b60")));
        mMap.addMarker(new MarkerOptions().position(new LatLng(39.328345, -76.621778)).title("Red Area").icon(BitmapDescriptorFactory.fromResource(R.drawable.blank_marker)));

        mMap.addCircle(new CircleOptions().center(new LatLng(39.347272, -76.685526)).radius(800).strokeColor(Color.parseColor("#00ff5b60")).fillColor(Color.parseColor("#90ff5b60")));
        mMap.addCircle(new CircleOptions().center(new LatLng(39.369837, -76.613808)).radius(600).strokeColor(Color.parseColor("#00ff5b60")).fillColor(Color.parseColor("#90ff5b60")));
        mMap.addCircle(new CircleOptions().center(new LatLng(39.350315, -76.561976)).radius(1200).strokeColor(Color.parseColor("#00ff5b60")).fillColor(Color.parseColor("#90ff5b60")));
        mMap.addCircle(new CircleOptions().center(new LatLng(39.299925, -76.654602)).radius(1000).strokeColor(Color.parseColor("#00ff5b60")).fillColor(Color.parseColor("#90ff5b60")));
        mMap.addCircle(new CircleOptions().center(new LatLng(39.252994, -76.630920)).radius(1000).strokeColor(Color.parseColor("#00ff5b60")).fillColor(Color.parseColor("#90ff5b60")));


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                System.out.println("LAT LONG : " + latLng);
                mMap.addMarker(new MarkerOptions().position(latLng).title("New Marker"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                System.out.println("CAMERA MOVE ");
                float zoom = mMap.getCameraPosition().zoom;
                System.out.println("zoom : " + zoom);

                center_latlong = mMap.getCameraPosition().target;
                System.out.println("center_latlong : " + center_latlong);
                selected_latlong_text_view.setText("" + center_latlong);

//                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) circle_view.getLayoutParams();
//                params.height = 130;
//                params.width = 130;
//                circle_view.setLayoutParams(params);

            }
        });


//        List<LatLng> list = new ArrayList<LatLng>();
//        list.add(new LatLng(-37.1886, 145.708));
//        list.add(new LatLng(-37.8361, 144.845));
//        list.add(new LatLng(-38.4034, 144.192));
//        list.add(new LatLng(-38.7597, 143.67));
//        list.add(new LatLng(-36.9672, 141.083));


        WeightedLatLng data1 = new WeightedLatLng(new LatLng(39.287371, -76.598798), 11.5 );
        WeightedLatLng data2 = new WeightedLatLng(new LatLng(39.283887, -76.641649), 00.5 );
        WeightedLatLng data3 = new WeightedLatLng(new LatLng(39.311168, -76.585407), 17.5 );
        WeightedLatLng data4 = new WeightedLatLng(new LatLng(39.313533, -76.655828), 20.5 );
        WeightedLatLng data5 = new WeightedLatLng(new LatLng(39.281308, -76.634840), 51.5 );
        ArrayList<WeightedLatLng> weightedLatLngs = new ArrayList<>();

        weightedLatLngs.add(data1);
        weightedLatLngs.add(data2);
        weightedLatLngs.add(data3);
        weightedLatLngs.add(data4);
        weightedLatLngs.add(data5);

        int[] colors = {
                Color.rgb(102, 225, 0), // green
                Color.rgb(255, 0, 0)    // red
        };

        float[] startPoints = {
                0.2f, 1f
        };

        Gradient gradient = new Gradient(colors, startPoints);

        mProvider = new HeatmapTileProvider.Builder().weightedData(weightedLatLngs).gradient(gradient).build();


        // Create a heat map tile provider, passing it the latlngs of the police stations.
//        mProvider = new HeatmapTileProvider.Builder()
//                .data(list)
//                .build();
        // Add a tile overlay to the map, using the heat map tile provider.
        TileOverlay mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));

    }


}
