package com.example.gaurk.secumap;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gaurk on 10/26/2017.
 */

public class Statistics_main extends AppCompatActivity {

    private BarChart chart;
    private BarDataSet Bardataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_main);

        BarChart chart = (BarChart) findViewById(R.id.statistics_barchart);

        chart.getXAxis().setDrawGridLines(false);
        chart.setGridBackgroundColor(Color.TRANSPARENT);


        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);


        ArrayList<BarEntry> BarEntry = new ArrayList<>();
        BarEntry.add(new BarEntry(620f, 0));
        BarEntry.add(new BarEntry(510f, 1));
        BarEntry.add(new BarEntry(582f, 2));
        BarEntry.add(new BarEntry(500f, 3));
        BarEntry.add(new BarEntry(514f, 4));
        BarEntry.add(new BarEntry(618f, 5));
        BarEntry.add(new BarEntry(742f, 6));
        BarEntry.add(new BarEntry(858f, 7));
        BarEntry.add(new BarEntry(1131f, 8));
        BarEntry.add(new BarEntry(1823f, 9));

        ArrayList<BarEntry> BarEntry2 = new ArrayList<>();
        BarEntry2.add(new BarEntry(195f, 0));
        BarEntry2.add(new BarEntry(186f, 1));
        BarEntry2.add(new BarEntry(149f, 2));
        BarEntry2.add(new BarEntry(149f, 3));
        BarEntry2.add(new BarEntry(157f, 4));
        BarEntry2.add(new BarEntry(181f, 5));
        BarEntry2.add(new BarEntry(116f, 6));
        BarEntry2.add(new BarEntry(183f, 7));
        BarEntry2.add(new BarEntry(128f, 8));
        BarEntry2.add(new BarEntry(266f, 9));


//        IBarDataSet dataSet = new BarDataSet(BarEntry, "Opioid Related");
//        IBarDataSet dataSet2 = new BarDataSet(BarEntry2, "Non Opioid Related");


        BarDataSet barDataSet1 = new BarDataSet(BarEntry, "Non Opioid Related");
//        BarDataSet barDataSet2 = new BarDataSet(BarEntry2, "Non Opioid Related");
        barDataSet1.setColor(Color.parseColor("#00ff00"));
//        barDataSet2.setColor(Color.parseColor("#00ff00"));

//        BarData barDatax = new BarData(barDataSet1, barDataSet2);
        BarData barDatax = new BarData(barDataSet1);
//        System.out.println("COLOR :" + Color.parseColor("#00ff00"));
//        dataSet.setValueTextColor(Color.parseColor("#00ff00"));
//

        ArrayList<String> labels = new ArrayList<>();
        labels.add("2007");
        labels.add("2008");
        labels.add("2009");
        labels.add("2010");
        labels.add("2011");
        labels.add("2012");
        labels.add("2013");
        labels.add("2014");
        labels.add("2015");
        labels.add("2016");


//        BarData data = new BarData(barDataSet1, barDataSet2);
        BarData data = new BarData(barDataSet1);


        chart.setData(data);
        chart.invalidate();

        chart.setScaleEnabled(false);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setData(barDatax);
//        chart.setDescription("Drug Abuse Statistics");
//        chart.setDescriptionTextSize(16f);
//        chart.setDescriptionPosition(420f,35f);

        Legend legend = chart.getLegend();
        legend.setTextSize(16f);

///////////////////////////////////////////////////////////////



        Bardataset = new BarDataSet(BarEntry, "Projects");

        BarData BARDATA = new BarData(labels, Bardataset);


        chart.setData(BARDATA);

        chart.animateY(3000);






    }
}
